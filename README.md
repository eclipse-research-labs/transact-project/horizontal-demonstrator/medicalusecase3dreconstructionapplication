# MedicalUseCase3DReconstructionApplication

This project holds information and videos for the TNO-ESI 3D reconstruction demo application for the TRANSACT horizontal demonstrator.
This application was created as research vehicle in the EU TRANSACT project : https://transact-ecsel.eu/


## Description
This demonstrator was built to explore telemetry-based performance observability and verification capabilities for use across the device-edge-cloud continuum. Furthermore, this demonstrator demonstrates service continuity solutions in response to the in TRANSACT developed Service continuity analysis and mitigation methodology. 

See also the intro presentation for context [Intro MedicalUseCase3DReconstructionApplication](https://gitlab.eclipse.org/eclipse-research-labs/transact-project/horizontal-demonstrator/medicalusecase3dreconstructionapplication/-/blob/main/20240409-TRANSACT-ITI-TNO_intro_demonstrator.pdf).

Videos are provided to showcase some the capabilities and applications of this 3D reconstructor application and demonstrator: [MedicalUseCaseApplicationVideos](https://gitlab.eclipse.org/eclipse-research-labs/transact-project/horizontal-demonstrator/medicalusecase3dreconstructionapplication/-/tree/main/MedicalUseCaseApplicationVideos)

**This demo application is provided AS-IS**: as an open access consolidation of the EU TRANSACT project.
**No further development** will be undertaken after the end of the EU TRANSACT project: June 1st, 2024.

For more information, consult the EU TRANSACT project website: https://transact-ecsel.eu/.


## Functionality and Architecture Overview
In this application, a user running an edge-based client (AppViewer) in a web browser can selectsa folder of 2D images for 3D reconstruction. The selected images are uploaded into an S3 bucket in the cloud. From there, a service called OpenMVG (Open Multiple View Geometry) matches the images and creates a sparse point cloud. This can be done for all images sequentially, or in batches that can be processed in parallel. In the latter case, state required to synchronize the parallel processing is stored in an instance of MariaDB. Once OpenMVG has finished processing all batches, the OpenMVS (Open Multi-view Stereo) service performs 3D reconstruction based on the resulting point cloud. The final reconstructed 3D model is sent back to the edge-based client and is displayed in the web browser.

The architecture of the open-source 3D reconstruction demonstrator is as follows. All software is deployed in containers using Docker to improve flexibility and portability. Both, the device tier and the cloud tier are computing clusters orchestrated by Kubernetes to automate container management tasks, such as scaling, (re-)deployment, and evolving services. Kubernetes also enables Horizontal Pod Autoscaling (HPA), automatically scaling the number of instances of OpenMVG, e.g. in response to high CPU utilization. Note that all platform infrastructure is open source. 

## Installation
For installation, see related 3Dreconstruct projects in https://gitlab.eclipse.org/eclipse-research-labs/transact-project/horizontal-demonstrator.

## Usage
For usage examples see folder [MedicalUseCaseApplicationVideos](https://gitlab.eclipse.org/eclipse-research-labs/transact-project/horizontal-demonstrator/medicalusecase3dreconstructionapplication/-/tree/main/MedicalUseCaseApplicationVideos). 


## Authors and acknowledgment
This work is done by Rajat Chawla, Sven Weiss, Benny Akesson, Teun Hendriks of TNO-ESI as part of the EU TRANSACT project.

This work was partially supported by [TRANSACT EU project](https://transact-ecsel.eu/) funding. TRANSACT has received funding from the ECSEL Joint Undertaking (JU) under grant agreement No 101007260. The JU receives support from the European Union’s Horizon 2020 research and innovation programme and Austria, Belgium, Denmark, Finland, Germany, Poland, Netherlands, Norway, and Spain.

## License
See License file for license conditions: [Licence](https://gitlab.eclipse.org/eclipse-research-labs/transact-project/horizontal-demonstrator/medicalusecase3dreconstructionapplication/-/blob/main/LICENSE)

